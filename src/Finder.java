public class Finder {
    public String[] sortArray(String wordArray[]) {
        //loops through the array
        for(int i = 0; i < wordArray.length; i++) {
            //loops through the words in the array
            for(int j=1; j< wordArray.length; j++) {
                //checks to see if position 0 is bigger than position 1 and so on
                if(wordArray[j-1].compareTo(wordArray[j]) > 0) {
                    //String becomes the first word
                    String temp = wordArray[j-1];
                    //word becomes position 1
                    wordArray[j-1] = wordArray[j];
                    //position 1 gets but into position 0
                    wordArray[j]=temp;
                }
            }
        }
        return wordArray;
    }

    //Find the word we are looking for
    public int binarySearch(String[] wordArray, String key, int bottomHalf, int topHalf) {
        int mid;
        //While bottom is less then half
        while(bottomHalf <= topHalf) {
            mid = bottomHalf + (topHalf - bottomHalf) / 2; // we make the middle in the middle of the array we have
            if(wordArray[mid].equals(key)) { //if the word is in the middle we get index
                return mid;
            }
            if(key.compareTo(wordArray[mid]) > 0) { //if the guess is greater than the word
                bottomHalf = mid + 1; //then the bottom half becomes that middle + 1
            } else{
                topHalf = mid - 1; // if the guess is less then the word we are looking for then it becomes the top half becomes 1 less the middle.
            }
        }
        return -1; //if we do not find it.
    }
}
