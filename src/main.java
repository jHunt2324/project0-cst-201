//Jeffrey Hunt
//Project0:Cst-201-Array Warmup
//This is all my own work
//I had some help from the slides and the internet
//Date: 9/19/2021

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class main {
    public static void main(String args[]) throws FileNotFoundException {
        //Initial variables
        String wordArray[] = new String[100]; // reads an array of 100 characters
        Scanner fin = new Scanner(new File("test.txt")); //reads in a text file
        String str; //declare initial string
        int m = 0;
        Scanner sc = new Scanner(System.in); //allows us to ask for user input
        Finder finder = new Finder();

        //fill our array with the text file
        while(fin.hasNext()) {
           str = fin.next();
            wordArray[m] = str;
            m++;
        }

        System.out.println("Unsorted Array");
       //print out the unsorted list
      for(int j = 0; j <100; j++) {
          System.out.print(wordArray[j] + " ");
      }

      //sort the array
      finder.sortArray(wordArray);

      System.out.println();
      System.out.println();

      System.out.println("Sorted Array");
      //print out the sorted array
        for(int j = 0; j <100; j++) {
            System.out.print(wordArray[j] + " ");
        }

        System.out.println();
        System.out.println();

        System.out.println("Binary Search");
        //start the binary search
        String key = "";
        //Binary Search
        while(!key.equals("0")) {
            //Binary Search Variables
            int bottomHalf = 0;
            int length = 100;
            int topHalf = length - 1;

            System.out.println("Enter a word");
            key = sc.nextLine();

            //do the binary search
            int index = finder.binarySearch(wordArray, key, bottomHalf, topHalf);

            if(key.equals("0")) {
                break;
            }

            //checks to make sure we found the element
            if(index ==-1) {
                System.out.println(key + " is not in list");
            } else {
                System.out.println(key + " is at index " + index);
            }

        }

        System.out.println("Goodbye");
    }
}




